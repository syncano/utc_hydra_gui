(function(){
	'use strict';


	/*
	*/
	var LoginForm = {
		$el: $('#login-form'),

		init: function(){
			this.show();
			$('.btn-login-button').on('click', LoginForm.checkForm.bind(LoginForm));
		},

		checkForm: function(e){
			e.preventDefault();
			var data = {
				login: $('#username').val(),
				password: $('#password').val()
			};
			$.post('php/checkLogin.php', data, function(res){
				if(res.status === true){
					this.hide();
					sessionStorage.setItem('loggedIn', true);
					Chat.show();
				} else {
					$('.text-danger', this.$el).show();
				}
			}.bind(this), 'json');
		},

		hide: function(){
			this.$el.hide();
		},

		show: function(){
			this.$el.show();
		}
	};


	/*
	*/
	var Chat = {
		$el: $('#app'),
		$body: $('.messages-list'),

		state: '',

		months: ['Jan', 'Feb', 'March', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

		hide: function(){
			this.$el.hide();
		},

		show: function(){
			$('.btn-logout').on('click', App.logout.bind(App));
			$('input[type=radio]').on('click', this.changeState.bind(this));
			this.$el.show();
			this.showLoader();
			this.hideTable();
			$('input[value=All]').click();
		},

		changeState: function(e){
			var $input = $(e.currentTarget);
			this.state = $input.val();
			this.loadMessages();
		},

		showLoader: function(){
			$('.loader', this.$el).show();
		},

		hideLoader: function(){
			$('.loader', this.$el).hide();
		},

		showTable: function(){
			$('table', this.$el).show();
		},

		hideTable: function(){
			$('table', this.$el).hide();
			$('.messages-list', this.$el).empty();
		},

		loadMessages: function(){
			$('.text-danger', this.$el).hide();
			this.hideTable();
			this.showLoader();

			$.getJSON('php/getMessages.php', {state: this.state}, function(res){
				this.hideLoader();
				if(res.result === 'OK'){
					var msgs = this.sortMessages(res.data);
					this.drawMessages(msgs);
				} else {
					$('.text-danger', this.$el).show();
				}
			}.bind(this));
		},

		sortMessages: function(msgs){
			return msgs.sort(function(a, b){
				if(a.createdAt < b.createdAt){
					return 1;
				} else if(a.createdAt > b.createdAt){
					return -1;
				}
				return 0;
			});
		},

		drawMessages: function(list){
			this.showTable();
			var h = [];
			var defaultClass = 'default';
			var cls;
			for(var i=0; i<list.length; i++){
				var rec = list[i];
				var state = rec.state;
				h.push('<tr class="' + state + '" id="record-' + rec.ID + '" rel="' + rec.folder + '">');
				h.push('<td>' + this.parseDate(rec.createdAt) + '</td>');
				h.push('<td><div>' + this.escape(rec.title) + '</div></td>');
				h.push('<td><div class="text">' + this.escape(rec.text) + '</div></td>');
				h.push('<td>');

				var p = rec.folder === 'INBOX' ? {cls:'default', txt: 'No'} : {cls: 'primary', txt: 'Yes'};
				h.push('<button class="btn btn-' + p.cls + ' btn-sm btn-highlight" type="button">' + p.txt + '</button>');

				h.push('</td>');
				h.push('<td>');
				h.push('<div class="btn-group">');

				cls = state === 'Pending' ? 'warning' : defaultClass;
				h.push('<button class="btn btn-' + cls + ' btn-sm Pending" type="button" title="Pending">Pending</button>');

				cls = state === 'Moderated' ? 'success' : defaultClass;
				h.push('<button class="btn btn-' + cls + ' btn-sm Moderated" type="button" title="Moderated">OK</button>');

				cls = state === 'Deleted' ? 'danger' : defaultClass;
				h.push('<button class="btn btn-' + cls + ' btn-sm Deleted" type="button" title="Deleted">Deleted</button>');

				h.push('</div>');
				h.push('</td>');
				h.push('</tr>');
			}
			$('.messages-list', this.$el).html(h.join('\n'));
			$('.btn-group button', this.$el).on('click', this.changeRecordState.bind(this));
			$('.btn-highlight', this.$el).on('click', this.changeHighlight.bind(this));
		},


		changeHighlight: function(e){
			e.preventDefault();
			var $btn = $(e.currentTarget);
			var $row = $btn.parent().parent();
			var id = $row.attr('id').split('-')[1];
			var folder = $row.attr('rel');
			var currentState = $row.get(0).className;

			if(folder === 'INBOX'){
				folder = 'highlights';
			} else {
				folder = 'INBOX';
			}

			$.get('php/highlightMessage.php', {id: id, folder: folder, state: currentState}, function(res){
				if(typeof res === 'string'){
					res = JSON.parse(res);
				}
				this.loadMessages();
			}.bind(this), 'json');
		},


		changeRecordState: function(e){
			e.preventDefault();
			var $btn = $(e.currentTarget);

			var $hiliteBtn = $btn.parent().parent().prev().find('button');

			// przycisk jest już zaznaczony, czyli rekord ma taką klasę, na jaką próbujemy zmienić. Nic nie rób.
			if($btn.hasClass('btn-default') === false){
				return;
			}

			var newState;
			var classes = ['Pending', 'Moderated', 'Deleted'];
			for(var i=0; i<classes.length; i++){
				if($btn.hasClass(classes[i])){
					newState = classes[i];
					break;
				}
			}

			var $row = $btn.parent().parent().parent();
			var id = $row.attr('id').split('-')[1];
			var folder = $row.attr('rel');

			$.post('php/setMessageState.php', {id: id, state: newState, folder: folder}, function(res){
				if(typeof res === 'string'){
					res = JSON.parse(res);
				}
				if(res.result === 'OK'){
					$btn.removeClass('btn-default').removeClass('btn-danger').removeClass('btn-warning').removeClass('btn-success');
					$btn.siblings().removeClass('btn-danger').removeClass('btn-warning').removeClass('btn-success').addClass('btn-default');
					if(newState === 'Pending'){
						$btn.addClass('btn-warning');
					} else if(newState === 'Moderated'){
						$btn.addClass('btn-success');
					} else if(newState === 'Deleted'){
						$btn.addClass('btn-danger');
					}

					if(newState !== 'Moderated'){
						$hiliteBtn.html('No').removeClass('btn-primary').addClass('btn-default');
					}
					$row.removeClass('Pending').removeClass('Moderated').removeClass('Deleted').addClass(newState);
					if(this.state !== 'All'){
						if(this.state !== newState){
							$row.fadeOut();
						}
					}
				}
			}.bind(this), 'json');
		},


		parseDate: function(date){
			if(typeof date === 'string'){
				date = new Date(date);
			}
			var d = date.getDate();
			d += d === 1 ? 'st' : d === 2 ? 'nd' : d === 3 ? 'rd' : 'th';
			return [
				this.months[date.getMonth()],
				d + ',',
				date.getHours() + ':' + this.zeroPad(date.getMinutes())
			].join(' ');
		},

		escape: function(s){
			if(typeof s === 'undefined'){
				return s;
			}
			s = s
				.replace(/&/g, '&amp;')
				.replace(/</g, '&lt;')
				.replace(/>/g, '&gt;')
				.replace(/"/g, '&quot;')
				.replace(/'/g, '&#x27;')
			;
			return s;
		},

		zeroPad: function(value){
			if(value < 10){
				return '0' + value;
			}
			return value;
		}
	};


	/*
	*/
	var App = {
		init: function(){
			var loggedIn = sessionStorage.getItem('loggedIn');
			if(loggedIn === 'true'){
				LoginForm.hide();
				Chat.show();
			} else {
				LoginForm.init();
			}
		},

		logout: function(e){
			e.preventDefault();
			sessionStorage.removeItem('loggedIn');
			Chat.hide();
			LoginForm.show();
		}
	};

	
	App.init();


})();