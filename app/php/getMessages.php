<?php
header('Content-Type: application/json');

function doCurl($url){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	return $res;
}

function makeUrl($state, $idx){
	$params = array(
		'login' => 'admin',
		'password' => 'hydraadmin2012',
		'method' => 'getData',
		'showId' => 850,
		'codeword' => 'chat_850',
		'folderName[]' => 'INBOX',
		'folderName[]' => 'highlights',
		'state' => $state,
		'limit' => 100,
		'startFrom' => $idx
	);
	$url = "http://hydrav2.active-loop.com/cod/getjson?";
	foreach($params as $key => $value) {
		$url .= $key . '=' . $value . '&';
	}
	return $url;
}

function getPack($state, $startIdx, $arr){
	$length = 100;
	$url = makeUrl($state, $startIdx);
	$json = doCurl($url);
	$res = json_decode($json, true);
	if($res['result'] === 'NOK'){
		return $res;
	}
	$cnt = count($res['data']);
	$res = array_merge($arr, $res['data']);
	if($cnt === $length){
		return getPack($state, $startIdx + $length - 1, $res);
	}
	return array('result' => 'OK', 'data' => $res);
}

$s = 'All';
if(isset($_GET['state'])){
	$s = $_GET['state'];
}

$res = getPack($s, 0, array());
if($res['result'] === 'OK'){
	$d = array();
	$ids = array();
	foreach($res['data'] as $key => $value){
		$id = $value['ID'];
		if(!in_array($id, $ids)){
			$ids[] = $id;
			$d[] = $value;
		}
	}
	$res['data'] = $d;
}
echo json_encode($res);
?>