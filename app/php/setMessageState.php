<?php
header('Content-Type: application/json');

function doCurl($url){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	return $res;
}

function makeUrl($id, $state, $folder){
	$params = array(
		'login' => 'admin',
		'password' => 'hydraadmin2012',
		'method' => 'moveData',
		'showId' => 850,
		'codeword' => 'chat_850',
		'codId' => $id,
		'state' => $state
	);

	// jeśli wiadomość była podświetlona, to znaczy, że była Moderated.
	// jeśli zmieniamy jej status, to znaczy, że trzeba ją przenieść z foldera highlights do INBOX
	if($folder == 'highlights'){
		$params['folderName'] = 'INBOX';
	}

	$url = "http://hydrav2.active-loop.com/cod/getjson?";
	foreach($params as $key => $value) {
		$url .= $key . '=' . $value . '&';
	}
	return $url;
}

if(!isset($_POST['id']) || !isset($_POST['state']) || !isset($_POST['folder'])){
	$res = array('result' => 'NOK');
} else {
	$id = $_POST['id'];
	$state = $_POST['state'];
	$folder = $_POST['folder'];
	$url = makeUrl($id, $state, $folder);
	$res = doCurl($url);
}

echo json_encode($res);
?>