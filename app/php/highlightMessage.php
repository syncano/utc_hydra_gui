<?php

function doCurl($url){
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$res = curl_exec($ch);
	curl_close($ch);
	return $res;
}

function makeUrl($params){
	$url = "http://hydrav2.active-loop.com/cod/getjson?";
	foreach($params as $key => $value) {
		$url .= $key . '=' . $value . '&';
	}
	return $url;
}

function getHighlightedMessage(){
	$params = array(
		'login' => 'admin',
		'password' => 'hydraadmin2012',
		'method' => 'getData',
		'showId' => 850,
		'codeword' => 'chat_850',
		'folderName' => 'highlights',
		'limit' => 1
	);
	$url = makeUrl($params);
	$json = doCurl($url);
	return json_decode($json, true);
}


function changeStateToModerated($id){
	$params = array(
		'login' => 'admin',
		'password' => 'hydraadmin2012',
		'method' => 'moveData',
		'showId' => 850,
		'codeword' => 'chat_850',
		'codId' => $id,
		'state' => 'Moderated',
	);
	$url = makeUrl($params);
	$json = doCurl($url);
	return json_decode($json, true);
}


function moveToFolder($id, $folder){
	$params = array(
		'login' => 'admin',
		'password' => 'hydraadmin2012',
		'method' => 'moveData',
		'showId' => 850,
		'codeword' => 'chat_850',
		'codId' => $id,
		'folderName' => $folder,
	);
	$url = makeUrl($params);
	$json = doCurl($url);
	return json_decode($json, true);
}

function moveMessagesToInbox(){
	// pobierz podświetloną wiadomość, o ile jest
	$msg = getHighlightedMessage();
	if(count($msg['data']) == 0){
		return array('result' => 'OK');
	} else {
		$id = $msg['data'][0]['ID'];
		return moveToFolder($id, 'INBOX');
	}
}


/* 
	w zmiennej folder mamy obecny folder. Czyli jeśli jest INBOX, to podświetlamy wiadomość, przesuwając ją do foldera highlights
	jeśli jest highlights, to zdejmujemy podświetlenie i przesuwamy do INBOX

	dodatkowo, gdy przenosimy do highlights to musimy pobrać wszystkie inne wiadomości z tego foldera i przenieść je do INBOX
*/
if(!isset($_GET['id']) || !isset($_GET['folder'])){
	$res = array('result' => 'NOK');
} else {
	$id = $_GET['id'];
	$folder = $_GET['folder'];
	$state = $_GET['state'];

	if($folder == 'highlights'){
		// podświetlamy wiadomość, więc najpierw trzeba przenieść wszystko z highlights do INBOX
		$res = moveMessagesToInbox();

		if($res['result'] == 'OK'){
			// jeśli wiadomość nie była Moderated, to najpierw trzeba jej zmienić stan
			if($state != 'Moderated'){
				$res = changeStateToModerated($id);
			}
			// teraz dopiero możemy podświetlić wiadomość
			if($res['result'] == 'OK'){
				$res = moveToFolder($id, $folder);
			}
		}
	} else {
		// zdejmujemy podświetlenie, więc wystarczy przenieść do INBOX
		$res = moveToFolder($id, $folder);
	}
}
echo json_encode($res);
?>